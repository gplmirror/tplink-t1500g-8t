TP-LINK T1500G-8T(UN) 1.0 GPL code readme

1. This package contains GPL code for T1500G-8T(UN) 1.0
2. All components have been built successfully on CentOS Linux release 6.0

Build Instructions
1. All build targets are in "t1500g-8t_gpl/tplink/buildroot-realtek/", you should enter the directory to build components.

2. Toolchain binaries are avaliable in this package. The directory is "t1500g-8t_gpl/tplink/buildroot-realtek/ext-tools/msdk-4.3.6-mips-EB-2.6.32-0.9.33/host/usr/bin/".

3. Building steps:
 1) put t1500g-8t_gpl in directory /project/trunk
 2) cd /project/trunk/t1500g-8t_gpl/tplink/buildroot-realtek
 3) make O=build/t1500g-8t tplink-t1500g-8t_defconfig
 4) make O=build/t1500g-8t
 After step4 completed, The uboot, linux kernel image, rootfs filesystem could be found in directory "t1500g-8t_gpl/tplink/buildroot-realtek/build/t1500g-8t/images".




