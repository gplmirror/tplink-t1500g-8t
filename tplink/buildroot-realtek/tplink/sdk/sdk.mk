#############################################################
#
# sdk 
#
#############################################################
SDK_VERSION = 1.0

SDK_MAKE_FLAGS = \
	HOSTCC="$(HOSTCC)" \
	HOSTCFLAGS="$(HOSTCFLAGS)" \
	ARCH=$(KERNEL_ARCH) \
	CROSS_COMPILE="$(CCACHE) $(TARGET_CROSS)" \
	SDK_TP_CFLAGS="$(BR2_TPLINK_CFLAGS_STRIP)"


SDK_LIB_INSTALL_PATH = $(BR2_TPLINK_SOURCE_DIR_STRIP)/archive/$(BR2_TPLINK_PROJECT_NAME_STRIP)

# override with board common rules if any
-include $(BR2_BOARD_COM_CFG_STRIP)/sdk.mk
# override with board specific rules if any
-include $(BR2_BOARD_DEF_CFG_STRIP)/sdk.mk

include tplink/sdk/sdk-last.mk
# do not include any makefile between sdk-last.mk and $(eval $(generic-package))
$(eval $(tplink-generic-package))

# 1. expand $(SDK_DIR) right now as we redefine $(BUILD_DIR) temporarily in tplink.mk during include process of tplink mk files
# 2. $(SDK_DIR) is exported
SDK_DIR := $(SDK_DIR)
export SDK_DIR

