################################################################################
#
# Linux kernel target
#
################################################################################

# you can override vars/cmds in linux/linux.mk

################################################################################
# vars/cmds
################################################################################
CONFIG_LINUXDIR	:= $(notdir $(LINUX_SITE))

LINUX_MAKE_FLAGS += KCONFIG_NOTIMESTAMP=1 -I$(SDKINCCOM)

PRODUCT_NAME		= 3.0.0

MACHARCH				= $(LINUX_DIR)/arch/mips/realtek
MACHINC				= $(LINUX_DIR)/include/asm-mips/mach-realtek
MACHARCH_REL      = $(CONFIG_LINUXDIR)/arch/mips/realtek
MACHINC_REL       = $(CONFIG_LINUXDIR)/include/asm-mips/mach-realtek
FLASH_SRC			= $(LINUX_DIR)/drivers/mtd/maps
FLASH_INC			= $(LINUX_DIR)/include/linux/mtd
FLASH_SRC_REL     = $(CONFIG_LINUXDIR)/drivers/mtd/maps
FLASH_INC_REL     = $(CONFIG_LINUXDIR)/include/linux/mtd

BSP_DIR				= $(LINUX_DIR)/arch/mips/bsp
TARGET_SYM_DIR 	= $(LINUX_DIR)/target
SDK_DRVDIR			= $(LINUX_DIR)/drivers/net/switch
SDK_RTCOREDIR		= $(SDK_DRVDIR)/rtcore
SDK_RTKDIR			= $(SDK_DRVDIR)/rtk
SDK_ETHDIR			= $(SDK_DRVDIR)/rtnic
SDK_NETDIR			= $(LINUX_DIR)/net/switch

FLASH_SRCDIR		= $(DRV_INCLUDE)/$(FLASH_SRC_REL)
FLASH_INCDIR		= $(DRV_INCLUDE)/$(FLASH_INC_REL)

# vars defined in tplink/sdk/sdk.mk
# SDK_DIR SDKINCCOM SDK_BUILD SDK_LIBDIR SDK_USRDIR DRV_INCLUDE

define LINUX_POST_BUILD_MOD_ROOTFS
	@cat $(TARGET_DIR)/etc/rc | sed 's/X.X.XX/$(LINUX_VERSION_PROBED)/g' > $(TARGET_DIR)/etc/rc.new
	@rm $(TARGET_DIR)/etc/rc
	@mv $(TARGET_DIR)/etc/rc.new $(TARGET_DIR)/etc/rc
	echo "Realtek/SDK Version 3.2.0 -- " `date` > $(TARGET_DIR)/etc/version
endef

# LINUX_POST_BUILD_MOD_ROOTFS is the first hook in LINUX_POST_BUILD_HOOKS in fact
LINUX_POST_BUILD_HOOKS ?= LINUX_POST_BUILD_MOD_ROOTFS

define LINUX_PRE_INSTALL_MKIMAGE
	# post_setup
	$(TARGET_OBJCOPY) -O binary -R .note -R .comment -S $(LINUX_DIR)/vmlinux $(LINUX_DIR)/vmlinux.bin
	# post_mkimage
	@cp -f $(LINUX_DIR)/vmlinux.bin $(LINUX_DIR)/vmlinux_org.bin
	@gzip -f -9 $(LINUX_DIR)/vmlinux_org.bin
	mkimage -A mips -O linux -T kernel -C gzip -a 0x80000000 -e 0x$(shell sed -n '/T kernel_entry/s/ T kernel_entry//p' $(LINUX_DIR)/System.map | sed -e 's/ffffffff//') -n "$(PRODUCT_NAME)" -d $(LINUX_DIR)/vmlinux_org.bin.gz $(LINUX_DIR)/vmlinux.bix
	@rm -f $(LINUX_DIR)/vmlinux_org.bin.gz
endef

define LINUX_INSTALL_IMAGES_CMDS
	$(LINUX_PRE_INSTALL_MKIMAGE)
	cp $(LINUX_DIR)/vmlinux.{bin,bix} $(BINARIES_DIR) 2>/dev/null || true
endef

################################################################################
# rules
################################################################################
.PHONY: linux.defconfig linux.oldconfig linux.flash.setup linux.bsp.setup \
        linux.sdk.setup linux.sdk.clean linux.target.setup

linux.defconfig: dirs linux-depends
	@echo "linux.defconfig prev #############################"
	@if [ ! -e $(LINUX_DIR)/.config ]; then \
		echo "cp linux default config"; \
		cp -f $(KERNEL_SOURCE_CONFIG) $(LINUX_DIR)/.config; \
	fi
	@echo "linux.defconfig post #############################"

linux.bsp.setup: dirs linux-depends sdk-depends
	@echo "linux.bsp.setup prev #############################"
	@if [ ! -e $(BSP_DIR) ]; then \
		echo "ln $(BSP_DIR)"; \
		ln -s ../../target/bsp $(BSP_DIR); \
	fi
	@echo "linux.bsp.setup post #############################"

linux.target.setup: dirs linux-depends sdk-depends
	@echo "linux.target.setup prev ##########################"
	@if [ ! -e $(TARGET_SYM_DIR) ]; then \
		echo "ln $(TARGET_SYM_DIR)"; \
		ln -s $(SDK_DIR)/system/linux/$(CONFIG_LINUXDIR)/arch/mips/realtek/sel_chip $(TARGET_SYM_DIR); \
	fi
	@echo "linux.target.setup post ##########################"

linux.oldconfig: linux.defconfig linux.bsp.setup linux.target.setup

linux.flash.setup: dirs linux-depends sdk-depends
	@echo "linux.flash.setup prev ###########################"
	@if [ ! -e $(FLASH_SRC)/rtk_spi_flash_mio.c ]; then \
		echo "ln rtk_spi_flash_mio.c"; \
		ln -s $(FLASH_SRCDIR)/rtk_spi_flash_mio.c $(FLASH_SRC)/; \
	fi
	@if [ ! -e $(FLASH_INC)/rtk_flash_common.h ]; then \
		echo "ln rtk_flash_common.h"; \
		ln -s $(FLASH_INCDIR)/rtk_flash_common.h $(FLASH_INC)/; \
	fi
	@if [ ! -e $(FLASH_INC)/rtk_spiflash_mio.h ]; then \
		echo "ln rtk_spiflash_mio.h"; \
		ln -s $(FLASH_INCDIR)/rtk_spiflash_mio.h $(FLASH_INC)/; \
	fi
	@echo "linux.flash.setup post ###########################"

linux.sdk.setup: linux.flash.setup
	@echo "linux.sdk.setup prev #############################"
	@if [ ! -e $(MACHARCH) ]; then \
		echo "ln $(MACHARCH)"; \
		ln -s $(SDK_DIR)/system/linux/$(MACHARCH_REL) $(MACHARCH); \
	fi
	@if [ ! -e $(MACHINC) ]; then \
		echo "ln $(MACHINC)"; \
		ln -s $(SDK_DIR)/system/linux/$(MACHINC_REL) $(MACHINC); \
	fi
	@echo "linux.sdk.setup post #############################"

linux-configure: linux.oldconfig sdk.defconfig

linux-build: linux-configure linux.sdk.setup sdk-configure
