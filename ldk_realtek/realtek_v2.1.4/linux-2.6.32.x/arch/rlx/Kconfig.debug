menu "Kernel hacking"

config TRACE_IRQFLAGS_SUPPORT
	bool
	default y

source "lib/Kconfig.debug"

config EARLY_PRINTK
	bool "Early printk" if EMBEDDED && DEBUG_KERNEL
	depends on SYS_HAS_EARLY_PRINTK
	default n
	help
	  This option enables special console drivers which allow the kernel
	  to print messages very early in the bootup process.

	  This is useful for kernel debugging when your machine crashes very
	  early before the console code is initialized. For normal operation,
	  it is not recommended because it looks ugly on some machines and
	  doesn't cooperate with an X server. You should normally say N here,
	  unless you want to debug such a crash.

config CMDLINE
	string "Default kernel command string"
	default ""
	help
	  On some platforms, there is currently no way for the boot loader to
	  pass arguments to the kernel. For these platforms, you can supply
	  some command-line options at build time by entering them here.  In
	  other cases you can specify kernel args so that you don't have
	  to set them up in board prom initialization routines.

config DEBUG_STACK_USAGE
	bool "Enable stack utilization instrumentation"
	depends on DEBUG_KERNEL
	help
	  Enables the display of the minimum amount of free stack which each
	  task has ever had available in the sysrq-T and sysrq-P debug output.

	  This option will slow down process creation somewhat.

config SMTC_IDLE_HOOK_DEBUG
	bool "Enable additional debug checks before going into CPU idle loop"
	depends on DEBUG_KERNEL && MIPS_MT_SMTC
	help
	  This option enables Enable additional debug checks before going into
	  CPU idle loop.  For details on these checks, see
	  arch/mips/kernel/smtc.c.  This debugging option result in significant
	  overhead so should be disabled in production kernels.

config SB1XXX_CORELIS
	bool "Corelis Debugger"
	depends on SIBYTE_SB1xxx_SOC
	select DEBUG_INFO
	help
	  Select compile flags that produce code that can be processed by the
	  Corelis mksym utility and UDB Emulator.

config RUNTIME_DEBUG
	bool "Enable run-time debugging"
	depends on DEBUG_KERNEL
	help
	  If you say Y here, some debugging macros will do run-time checking.
	  If you say N here, those macros will mostly turn to no-ops.  See
	  arch/mips/include/asm/debug.h for debugging macros.
	  If unsure, say N.

endmenu
